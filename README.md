# Application summary
Hi!

This is my first client-side application made during the education of Ethyrium smart contracts creation. 

# Installation and deployment

- run "npm install" from the repository root folder
- create .env file inside the repository root folder and add the next env variables:
  - MNEMONIC="%Your test account mnemonic%"
  - INFURA="https://rinkeby.infura.io/v3/%Your Infura token%"
- run "node deploy" there
- copy and save from the output:
  - contract address
  - contract interface for the Web3 provider

Done!