const path = require("path");
const fs = require("fs");
const solc = require("solc");
const contractName = "Chatroom";
const fileName = `${contractName}.sol`;

const contractPath = path.resolve(__dirname, "contracts", fileName);
const source = fs.readFileSync(contractPath, "UTF-8");

const input = {
  language: "Solidity",
  sources: {
    [`${fileName}`]: {
      content: source
    }
  },
  settings: {
    outputSelection: {
      "*": {
        "*": ["*"]
      }
    }
  }
};

const output = JSON.parse(solc.compile(JSON.stringify(input)));

exports.abi = output.contracts[fileName][contractName].abi;
exports.bytecode = output.contracts[fileName][contractName].evm.bytecode.object;
