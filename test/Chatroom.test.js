const assert = require("assert");
const chai = require("chai");
const ganache = require("ganache-cli");
const Web3 = require("web3");
const web3 = new Web3(ganache.provider());
const { abi, bytecode } = require("../compile");

chai.should();

let accounts;
let chatroom;
const gas = "3000000";

before(async () => {
  accounts = await web3.eth.getAccounts();
  chatroom = await new web3.eth.Contract(abi)
    .deploy({
      data: bytecode,
      arguments: ["Hi there!"]
    })
    .send({ from: accounts[0], gas });
});

describe("Chatroom", () => {
  it("deploy a contract", () => {
    console.log(chatroom.options.address);
    assert.ok(chatroom.options.address);
  });

  it("register a new user", async () => {
    await chatroom.methods.signup("testuser").send({ from: accounts[0], gas });
    await chatroom.methods.signup("testuser1").send({ from: accounts[1], gas });
  });

  it("logged in user profile", async () => {
    const res = await chatroom.methods.getUser().call({ from: accounts[0] });
    res.name.should.equal("testuser");
    res.registered.should.equal(true);
  });

  it("should not allow to register twice with the same account", async () => {
    let error;
    try {
      await chatroom.methods
        .signup("testuser1")
        .send({ from: accounts[0], gas });
    } catch (e) {
      error = e.message;
    }
    assert.ok(error);
    assert.equal(
      error,
      "VM Exception while processing transaction: revert You are already registered"
    );
  });

  it("should not allow to register with the existing name", async () => {
    let error;
    try {
      await chatroom.methods
        .signup("testuser")
        .send({ from: accounts[2], gas });
    } catch (e) {
      error = e.message;
    }
    assert.ok(error);
    assert.equal(
      error,
      "VM Exception while processing transaction: revert This nickname is already picked"
    );
  });

  it("list users", async () => {
    const res = await chatroom.methods.namesList().call();
    res.should.be.a("array").and.include("testuser", "testuser1");
    res.length.should.equal(2);
  });

  it("send a message", async () => {
    await chatroom.methods.send("Hi there").send({ from: accounts[0], gas });
    await chatroom.methods.send("Hello!").send({ from: accounts[1], gas });
    await chatroom.methods.send("Hello1!").send({ from: accounts[1], gas });
    await chatroom.methods.send("Hello2!").send({ from: accounts[1], gas });
    await chatroom.methods.send("Hello3!").send({ from: accounts[1], gas });
    await chatroom.methods.send("Hello4!").send({ from: accounts[1], gas });
    await chatroom.methods.send("Hello5!").send({ from: accounts[1], gas });
    await chatroom.methods.send("Hello6!").send({ from: accounts[1], gas });
    await chatroom.methods.send("Hello7!").send({ from: accounts[1], gas });
    await chatroom.methods.send("Hello8!").send({ from: accounts[1], gas });
    await chatroom.methods.send("Hello9!").send({ from: accounts[1], gas });
    await chatroom.methods.send("Hello10!").send({ from: accounts[1], gas });
    await chatroom.methods.send("Hello11!").send({ from: accounts[1], gas });
  });

  it("list messages", async () => {
    const length = 5;

    let res = await chatroom.methods.getMessages(-1).call();
    res.should.be.a("object");
    let messages = res[0];
    let message = messages[length - 1];
    messages.length.should.equal(length);
    message.text.should.equal("Hello11!");
    message.should.include.keys("name", "text", "timestamp", "number");

    let cursor = res[1];
    cursor.should.equal("3");
    res = await chatroom.methods.getMessages(cursor).call();
    messages = res[0];
    message = messages[length - 1];
    messages.length.should.equal(length);
    message.text.should.equal("Hello6!");
    message.should.include.keys("name", "text", "timestamp", "number");

    cursor = res[1];
    cursor.should.equal("0");
  });
});
